export const environment = {
	production: true,
	api_url: ' ',
	api_version: 'api/v1/',
	api_key: '',
	api_secret_key: '',
	googleWebClient: '',
	googleAndroidClient: '',
	REVERSED_CLIENT_ID: '',
	googleBrowserKey: '',
	facebookAppID: '',
	facebookAppSecret: '',
	httpPrefix: 'http:',
	sendBird_API_ID: '',
	OneSignal_GoogleID: '',
	OneSignal_AppID: ''
};
