import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ModalController,
  ViewController
} from "ionic-angular";

import { FileuploadPage } from "../../pages/fileupload/fileupload";
import { ModalFormPage } from "../../pages/modal-form/modal-form";

@Component({
  selector: "inspection-popover",
  templateUrl: "inspection-popover.html"
})
export class InspectionPopoverComponent {
  inspection: any = {};
  checklist: any = {};

  constructor(
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController
  ) {
    this.inspection = this.navParams.get("inspection");
    this.checklist = this.navParams.get("checklist");
  }

  uploadFile() {
    this.viewCtrl.dismiss();
    const modal = this.modalCtrl.create(FileuploadPage, {
      inspection: this.inspection
    });
    modal.onDidDismiss(() => {
      // Do something here
    });
    modal.present();
  }

  openRemarks() {
    this.viewCtrl.dismiss();
    const modal = this.modalCtrl.create(
      ModalFormPage,
      {
        params: this.checklist,
        action: "remarks_entry",
        mode: "create"
      },
      {
        cssClass: "remarks-modal"
      }
    );
    modal.onDidDismiss(() => {
      // Do something here
    });
    modal.present();
  }
}
