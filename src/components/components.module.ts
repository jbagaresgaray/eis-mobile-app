import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";
import { NgxQRCodeModule } from "ngx-qrcode2";
import { IonicImageViewerModule } from "ionic-img-viewer";

import { MdiIconComponent } from "./mdi-icon/mdi-icon";
import { SkeletonItemComponent } from "./skeleton-item/skeleton-item";

import { InspectionDetailsComponent } from "./form-content/inspection-details/inspection-details";
import { ReportDetailsComponent } from "./form-content/report-details/report-details";
import { ReportDetailsScanComponent } from "./form-content/report-details-scan/report-details-scan";

import { InspectionEntryComponent } from "./form-entry/inspection-entry/inspection-entry";
import { ReportEntryComponent } from "./form-entry/report-entry/report-entry";
import { RemarkEntryComponent } from "./form-entry/remark-entry/remark-entry";
import { InspectionPopoverComponent } from "./inspection-popover/inspection-popover";

@NgModule({
  declarations: [
    MdiIconComponent,
    SkeletonItemComponent,
    InspectionDetailsComponent,
    InspectionEntryComponent,
    ReportDetailsComponent,
    ReportEntryComponent,
    RemarkEntryComponent,
    InspectionPopoverComponent,
    ReportDetailsScanComponent
  ],
  entryComponents: [
    MdiIconComponent,
    SkeletonItemComponent,
    InspectionDetailsComponent,
    InspectionEntryComponent,
    ReportDetailsComponent,
    ReportEntryComponent,
    RemarkEntryComponent,
    InspectionPopoverComponent,
    ReportDetailsScanComponent
  ],
  imports: [IonicModule, NgxQRCodeModule, IonicImageViewerModule],
  exports: [
    MdiIconComponent,
    SkeletonItemComponent,
    InspectionDetailsComponent,
    InspectionEntryComponent,
    ReportDetailsComponent,
    ReportEntryComponent,
    RemarkEntryComponent,
    InspectionPopoverComponent,
    ReportDetailsScanComponent
  ]
})
export class ComponentsModule {}
