import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter
} from "@angular/core";
import {
  NavController,
  NavParams,
  Events,
  ActionSheetController,
  AlertController,
  LoadingController
} from "ionic-angular";
import * as _ from "lodash";
import * as moment from "moment";

import { FormEntryPage } from "../../../pages/form-entry/form-entry";
import { StorageProvider } from "../../../providers/storage/storage";

@Component({
  selector: "inspection-details",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "inspection-details.html"
})
export class InspectionDetailsComponent implements OnInit {
  @Input() inspection: any = {};
  @Input() refresh: EventEmitter<any>;
  @Input() viewWillEnter: EventEmitter<any>;

  details: any = {};
  equipmentsArr: any[] = [];
  fakeArr: any[] = [];

  showContent: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private storage: StorageProvider
  ) {
    for (let index = 0; index < 20; index++) {
      this.fakeArr.push(index);
    }
  }

  ngOnInit() {
    this.refresh.subscribe(data => {
      this.initData(data);
    });

    this.viewWillEnter.subscribe(() => {
      if (this.inspection) {
        this.initData();
      }
    });
  }

  private initData(ev?: any) {
    this.showContent = false;

    if (ev) {
      this.events.publish("sync:data:inspection_detail", this.inspection.id);
    }

    this.events.subscribe("sync:data:inspection_detail:complete", () => {
      getInspectionDetails();

      if (ev) {
        this.showContent = true;
        ev.complete();
      }
    });

    const getInspectionDetails = () => {
      this.storage
        .getStorage(
          this.storage.storageKey.INSPECTION_DETAIL + "_" + this.inspection.id
        )
        .then((details: any) => {
          if (details) {
            this.details = details;
            this.equipmentsArr = details.equipments;
            _.each(this.equipmentsArr, (row: any) => {
              row.inspection_date_formatted = moment(
                row.inspection_date
              ).format("MMM DD");

              if (row.status === "In progress") {
                row.color = null;
                row.note_color = "j3secondary";
              } else if (row.status === "Pending Inspection") {
                row.color = "j3lightwarning";
                row.note_color = "j3warning";
              } else if (row.status === "Inspection Finished") {
                row.color = null;
                row.note_color = "j3note";
              }

              const storeKey =
                this.storage.storageKey.EQUIPMENT_CHECKLIST +
                "_" +
                row.inspection_id +
                "_" +
                row.business_id +
                "_" +
                row.equipment_id;

              this.storage
                .getStorage(
                  this.storage.storageKey.CHECKLIST_DRAFT + "-" + storeKey
                )
                .then((draft: any) => {
                  if (draft) {
                    row.hasDraft = true;
                  } else {
                    row.hasDraft = false;
                  }
                });
            });
            console.log("this.equipmentsArr: ", this.equipmentsArr);
          }
          this.showContent = true;
        });
    };

    getInspectionDetails();
  }

  private refresDetails = resp => {
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.events.publish("sync:data:inspection_detail", this.inspection.id);
        this.initData(null);
        resolve();
      }
    });
  };

  viewDetails(equipment: any, event) {
    event.preventDefault();
    event.stopPropagation();

    this.navCtrl.push(FormEntryPage, {
      action: "inspection_details_entry",
      equipment: equipment,
      inspection: this.inspection,
      callback: this.refresDetails
    });
  }

  presentActionSheet(equipment: any, event: any) {
    event.preventDefault();
    event.stopPropagation();

    const deleteEquipment = () => {};

    const actionSheet = this.actionSheetCtrl.create({
      title: "Choose action for this Equipment",
      buttons: [
        {
          text: "Delete",
          role: "destructive",
          handler: () => {
            const confirm = this.alertCtrl.create({
              title: "Delete this equipment?",
              message: "Confirm to delete this equipment?",
              buttons: [
                {
                  text: "Cancel",
                  handler: () => {}
                },
                {
                  text: "Confirm",
                  handler: () => {
                    deleteEquipment();
                  }
                }
              ]
            });
            confirm.present();
          }
        },
        {
          text: "Update",
          handler: () => {
            console.log("Archive clicked");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
}
