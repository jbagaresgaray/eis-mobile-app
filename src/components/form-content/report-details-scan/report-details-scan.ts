import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { NavController, NavParams, Platform } from "ionic-angular";
import * as _ from "lodash";
import * as moment from "moment";

import { InspectionProvider } from "../../../providers/inspection/inspection";
import { NetworkProvider } from "../../../providers/network/network";
import { StorageProvider } from "../../../providers/storage/storage";
@Component({
  selector: "report-details-scan",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "report-details-scan.html"
})
export class ReportDetailsScanComponent implements OnInit {
  params: any = {};
  details: any = {};
  qrvalue: any;

  equipmentsArr: any[] = [];

  showContent: boolean;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public inspectionService: InspectionProvider,
    public network: NetworkProvider,
    public storage: StorageProvider
  ) {}

  ngOnInit() {
    if (this.navParams.get("params")) {
      this.qrvalue = this.navParams.get("params");
      this.params = JSON.parse(this.navParams.get("params"));
      console.log("params: ", this.params);

      this.platform.ready().then(() => {
        if (this.platform.is("cordova")) {
          this.network.getNetworkStatus().subscribe(data => {
            console.log("getNetworkStatus", data);
            if (data === 0) {
              this.initData(this.params.id);
              // this.initOfflineData(this.params.id);
            } else {
              this.initOfflineData(this.params.id);
            }
          });
        } else {
          this.initData(this.params.id);
          // this.initOfflineData(this.params.id);
        }
      });
    }
  }

  private initData(inspectionId) {
    this.showContent = false;
    this.inspectionService.getInspectionInfo(inspectionId).then((data: any) => {
      const resp = data.response;
      if (resp && resp.data) {
        console.log("resp.data: ", resp.data.resp);
        const inspections = resp.data.resp;
        console.log("inspections: ", inspections);
        if (!_.isEmpty(inspections)) {
          this.details = inspections;
          this.details.equipment_count = _.size(this.details.equipments);
          this.details.inspection_date =
            this.details.inspection_date || this.params.inspection_date;

          console.log("this.details: ", this.details);
          this.equipmentsArr = inspections.equipments;
          _.each(this.equipmentsArr, (row: any) => {
            row.inspection_date_formatted = moment(row.inspection_date).format(
              "MMM DD"
            );

            if (row.status === "In progress") {
              row.color = null;
              row.note_color = "j3secondary";
            } else if (row.status === "Pending Inspection") {
              row.color = "j3lightwarning";
              row.note_color = "j3lightwarning";
            } else if (row.status === "Inspection Finished") {
              row.color = null;
              row.note_color = "j3note";
            }
          });
          console.log("this.equipmentsArr: ", this.equipmentsArr);
        }
      }
      this.showContent = true;
    });
  }

  private initOfflineData(inspectionId) {
    const keyId =
      this.storage.storageKey.INSPECTION_DETAIL + "_" + inspectionId;
    console.log("initOfflineData: ", keyId);

    this.storage.getStorage(keyId).then((details: any) => {
      if (details) {
        this.details = details;
        console.log("this.details: ", this.details);
        this.equipmentsArr = details.equipments;
        _.each(this.equipmentsArr, (row: any) => {
          row.inspection_date_formatted = moment(row.inspection_date).format(
            "MMM DD"
          );

          if (row.status === "In progress") {
            row.color = null;
            row.note_color = "j3secondary";
          } else if (row.status === "Pending Inspection") {
            row.color = "j3lightwarning";
            row.note_color = "j3lightwarning";
          } else if (row.status === "Inspection Finished") {
            row.color = null;
            row.note_color = "j3note";
          }
        });
        console.log("this.equipmentsArr: ", this.equipmentsArr);
      }
      this.showContent = true;
    });
  }
}
