import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { FormEntryPage } from "../../../pages/form-entry/form-entry";

@Component({
  selector: "report-details",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "report-details.html"
})
export class ReportDetailsComponent implements OnInit {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ngOnInit() {}

  viewDetails() {
    this.navCtrl.push(FormEntryPage, {
      action: "report_details_entry"
    });
  }
}
