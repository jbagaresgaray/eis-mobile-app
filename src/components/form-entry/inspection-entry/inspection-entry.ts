import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  ViewChild,
  NgZone,
  Output
} from "@angular/core";
import {
  NavController,
  NavParams,
  ModalController,
  Slides,
  ActionSheetController,
  PopoverController,
  Platform,
  AlertController,
  LoadingController,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";
import * as moment from "moment";
import * as async from "async";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { FileuploadPage } from "../../../pages/fileupload/fileupload";
import { ModalFormPage } from "../../../pages/modal-form/modal-form";
import { InspectionPopoverComponent } from "../../inspection-popover/inspection-popover";

import { StorageProvider } from "../../../providers/storage/storage";
import { InspectionProvider } from "../../../providers/inspection/inspection";
import { InspectionAssetsProvider } from "../../../providers/inspection-assets/inspection-assets";

import { Network } from "@ionic-native/network";
import { ChecklistInputProvider } from "../../../providers/checklist-input/checklist-input";

import { environment } from "../../../environments/environment";

@Component({
  selector: "inspection-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "inspection-entry.html"
})
export class InspectionEntryComponent implements OnInit {
  @Input() inspection: any = {};
  @Input() equipment: any = {};
  @Input() create: EventEmitter<any>;
  @Input() saveLocal: EventEmitter<any>;
  @Input() nextSlide: EventEmitter<any>;
  @Input() prevSlide: EventEmitter<any>;
  @Input() viewWillLeave: EventEmitter<any>;

  @Output() formBroadcast = new EventEmitter();
  @Output() viewDidLeave = new EventEmitter();

  inspectionForm: FormGroup;

  isFormChanges: boolean;
  showContent: boolean;
  isSaving = false;
  submitted = false;

  storeKey: string;
  checklistKey: string;
  checklistInputKey: string;
  checklistInputImageKey: string;

  isProgress: boolean;
  disableProgress: boolean;

  inspection_info: any = {};
  business_equipment_basic_info: any = {};

  checklistArr: any[] = [];
  equipment_form_categoryArr: any[] = [];
  inspection_input: any[] = [];
  checklistInputArr: any[] = [];

  callback: any;

  @ViewChild(Slides) slides: Slides;

  constructor(
    public zone: NgZone,
    public platform: Platform,
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public popoverCtrl: PopoverController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private storage: StorageProvider,
    private inspectionService: InspectionProvider,
    private inspectionAssets: InspectionAssetsProvider,
    private inspectionChecklist: ChecklistInputProvider,
    private network: Network
  ) {
    this.inspection.date = new Date();
  }

  ngOnInit() {
    this.isFormChanges = false;
    this.isProgress = false;
    this.disableProgress = true;
    this.callback = this.navParams.get("callback");

    if (this.inspection) {
      this.inspection.date = new Date();
    }

    if (this.equipment) {
      this.showContent = false;

      this.storeKey =
        this.storage.storageKey.EQUIPMENT_CHECKLIST +
        "_" +
        this.inspection.id +
        "_" +
        this.inspection.business_id +
        "_" +
        this.equipment.equipment_id;
      console.log("storeKey: ", this.storeKey);

      this.checklistKey =
        this.storage.storageKey.CHECKLISTS +
        "_" +
        this.inspection.id +
        "_" +
        this.inspection.business_id +
        "_" +
        this.equipment.equipment_id;
      console.log("checklistKey: ", this.checklistKey);

      this.checklistInputKey =
        this.storage.storageKey.CHECKLISTS_INPUT +
        "_" +
        this.inspection.id +
        "_" +
        this.inspection.business_id +
        "_" +
        this.equipment.equipment_id;
      console.log("checklistInputKey: ", this.checklistInputKey);

      this.getDraftInspectionBusinessEquipment();
    }

    this.create.subscribe(() => {
      this.saveEntry();
    });

    this.saveLocal.subscribe(() => {
      this.saveLocally();
    });

    this.nextSlide.subscribe(() => {
      this.slides.slideNext();
    });

    this.prevSlide.subscribe(() => {
      this.slides.slidePrev();
    });
  }

  private onFormChanges(): void {
    this.isFormChanges = true;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private getDraftInspectionBusinessEquipment() {
    this.formBroadcast.emit({
      action: "form-loading-start",
      params: {}
    });
    this.storage
      .getStorage(this.storage.storageKey.CHECKLIST_DRAFT + "-" + this.storeKey)
      .then((draft: any) => {
        if (draft) {
          const _draft = JSON.parse(draft);
          console.log("draft: ", _draft);
          this.business_equipment_basic_info =
            _draft.business_equipment_basic_info;
          this.inspection_info = _draft.inspection_info;

          if (
            this.inspection_info &&
            this.inspection_info.request_status === "Inspection Finished"
          ) {
            this.isProgress = true;
            this.disableProgress = true;
          } else if (
            this.inspection_info &&
            this.inspection_info.request_status === "In progress"
          ) {
            this.isProgress = true;
            this.disableProgress = false;
          } else {
            this.isProgress = false;
            this.disableProgress = false;
          }

          let checklist = _draft.checklist;
          const equipment_form_categoryArr = _.uniqBy(
            _.map(checklist, (row: any) => {
              return {
                equipment_form_category_id: row.equipment_form_category_id,
                equipment_form_category_name: row.equipment_form_category_name,
                show: true
              };
            }),
            "equipment_form_category_id"
          );

          _.each(equipment_form_categoryArr, (row: any) => {
            row.checkList = _.filter(checklist, (col: any) => {
              return (
                col.equipment_form_category_id == row.equipment_form_category_id
              );
            });
          });

          this.equipment_form_categoryArr = equipment_form_categoryArr;
          this.checklistArr = checklist;
          console.log(
            "equipment_form_categoryArr: ",
            equipment_form_categoryArr
          );
          console.log("checklistArr: ", this.checklistArr);

          this.formBroadcast.emit({
            action: "form-loading-end",
            params: {}
          });

          this.zone.run(() => {
            this.showContent = true;
          });
        } else {
          async.waterfall([
            callback => {
              this.getInspectionBusinessEquipment().then(() => {
                callback();
              });
            },
            callback => {
              /* this.getEquipmentChecklistInput().then(() => {
                callback();
              }); */
              callback();
            }
          ]);
        }
      });
  }

  private async getInspectionBusinessEquipment() {
    this.formBroadcast.emit({
      action: "form-loading-start",
      params: {}
    });

    await this.storage.getStorage(this.storeKey).then((data: any) => {
      console.log("getInspectionBusinessEquipment: ", data);
      if (data) {
        const business_equipment_basic_info =
          data.business_equipment_basic_info;
        const inspection_info = data.inspection_info;

        if (
          inspection_info &&
          inspection_info.request_status === "Inspection Finished"
        ) {
          this.isProgress = true;
          this.disableProgress = true;
        } else if (
          inspection_info &&
          inspection_info.request_status === "In progress"
        ) {
          this.isProgress = true;
          this.disableProgress = false;
        } else {
          this.isProgress = false;
          this.disableProgress = false;
        }

        const inspection_date = new Date(inspection_info.inspection_date);
        inspection_info.inspection_date = new Date(
          inspection_date.getTime() -
            inspection_date.getTimezoneOffset() * 60000
        ).toJSON();
        this.business_equipment_basic_info = business_equipment_basic_info;
        this.inspection_info = inspection_info;
      }
      this.formBroadcast.emit({
        action: "form-loading-end",
        params: {}
      });

      this.zone.run(() => {
        this.showContent = true;
      });
    });

    this.storage.getStorage(this.checklistKey).then((data: any) => {
      let checklist = data.data;
      const equipment_form_categoryArr = _.uniqBy(
        _.map(checklist, (row: any) => {
          return {
            equipment_form_category_id: row.equipment_form_category_id,
            equipment_form_category_name: row.equipment_form_category_name,
            show: true
          };
        }),
        "equipment_form_category_id"
      );

      _.each(checklist, (row: any) => {
        row.uploaded = [];
        row.inspection_equipment_id = data.inspection_equipment_id;
        row.option_group_items = _.orderBy(
          row.option_group_items,
          ["value"],
          ["asc"]
        );
      });

      _.each(equipment_form_categoryArr, (row: any) => {
        row.checkList = _.filter(checklist, (col: any) => {
          return (
            col.equipment_form_category_id == row.equipment_form_category_id
          );
        });
      });

      this.equipment_form_categoryArr = equipment_form_categoryArr;
      this.checklistArr = checklist;
      console.log("equipment_form_categoryArr: ", equipment_form_categoryArr);
      console.log("checklistArr: ", this.checklistArr);
    });

    this.storage.getStorage(this.checklistInputKey).then((data: any) => {
      console.log("checklist input: ", data);
      const checklistInputArr = data.inspectioninputs.resp;
      console.log("checklistInputArr: ", checklistInputArr);

      async.eachSeries(
        checklistInputArr,
        (item, callback) => {
          const storeKey1 =
            this.storage.storageKey.CHECKLISTS_INPUT_IMAGE + "_" + item.id;
          console.log("storeKey1 assets: ", storeKey1);

          this.storage.getStorage(storeKey1).then((assets: any) => {
            if (assets) {
              item.uploaded = JSON.parse(assets);
            } else {
              item.uploaded = [];
            }
            console.log("item.uploaded: ", item.uploaded);
            callback();
          });
        },
        () => {
          this.checklistInputArr = checklistInputArr;
          _.each(this.checklistArr, (row: any) => {
            const result = _.find(this.checklistInputArr, {
              equipment_form_id: row.equipment_form_id
            });
            if (result) {
              row.value = result.value;
              row.remarks = result.remarks;
              row.uploaded = result.uploaded;
            } else {
              row.value = "";
              row.remarks = "";
              row.uploaded = [];
            }
          });
          console.log("this.checklistArr: ", this.checklistArr);
        }
      );
    });
  }

  private async getEquipmentChecklistInput() {
    await this.inspectionChecklist
      .getEquipmentChecklistInput(
        this.inspection.id,
        this.inspection.business_id,
        this.equipment.equipment_id
      )
      .then((data: any) => {
        const resp = data.response.data;
        this.checklistInputArr = resp.inspectioninputs.resp;
        console.log("checklist input: ", this.checklistInputArr);

        async.eachSeries(
          this.checklistInputArr,
          (item, callback) => {
            this.inspectionAssets
              .getAllInspectionInputAssets(item.id)
              .then((assets: any) => {
                item.uploaded = assets.response.data.resp;
                _.each(item.uploaded, (row: any) => {
                  row.public_path = environment.api_url + "/" + row.path;
                });
                callback();
              });
          },
          () => {
            console.log("his.checklistInputArr = ", this.checklistInputArr);
            _.each(this.checklistArr, (row: any) => {
              const result = _.find(this.checklistInputArr, {
                equipment_form_id: row.equipment_form_id
              });
              if (result) {
                row.value = result.value;
                row.remarks = result.remarks;
                row.uploaded = result.uploaded;
              } else {
                row.value = "";
                row.remarks = "";
                row.uploaded = [];
              }
            });
          }
        );
      });
  }

  updateInspectionStatus(value) {
    console.log("updateInspectionStatus: ", this.isProgress);
    if (!this.isProgress) {
      let alert = this.alertCtrl
        .create({
          title: "You cannot revert to previous status",
          subTitle:
            "Once In Progress is active, you cannot revert back to Pending status",
          buttons: ["CLOSE"]
        })
        .present();
      alert.then(() => {
        this.isProgress = true;
      });
      return;
    }
  }

  uploadFile(checklist: any) {
    const modal = this.modalCtrl.create(FileuploadPage, {
      inspection: this.inspection,
      checklist: checklist
    });
    modal.onDidDismiss((resp: any) => {
      // Do something here
      if (resp) {
        if (resp.action == "save") {
          if (resp.uploaded) {
            _.each(resp.uploaded, row => {
              checklist.uploaded.push(row);
            });
          } else {
            checklist.uploaded = [];
          }
        }
      }
      console.log("checklist: ", checklist);
    });
    modal.present();
  }

  openRemarks(item: any) {
    const modal = this.modalCtrl.create(
      ModalFormPage,
      {
        params: item,
        action: "remarks_entry",
        mode: "create"
      },
      {
        cssClass: "remarks-modal"
      }
    );
    modal.present();
  }

  showMoreActions(item: any, event: any) {
    const popover = this.popoverCtrl.create(InspectionPopoverComponent, {
      inspection: this.inspection,
      checklist: item
    });

    const actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: "Remarks",
          handler: () => {
            console.log("Destructive clicked");
            this.openRemarks(item);
          }
        },
        {
          text: "Upload",
          handler: () => {
            console.log("Archive clicked");
            this.uploadFile(item);
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });

    if (this.platform.is("ios")) {
      actionSheet.present();
    } else if (this.platform.is("android")) {
      popover.present({
        ev: event
      });
    } else {
      popover.present({
        ev: event
      });
    }
  }

  saveLocally() {
    console.log(
      "business_equipment_basic_info: ",
      this.business_equipment_basic_info
    );
    console.log("inspection_info: ", this.inspection_info);
    console.log("checklistArr: ", this.checklistArr);

    const saveDraft = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();

      this.storage
        .setStorage(
          this.storage.storageKey.CHECKLIST_DRAFT + "-" + this.storeKey,
          JSON.stringify({
            key: this.storeKey,
            business_equipment_basic_info: this.business_equipment_basic_info,
            inspection_info: this.inspection_info,
            checklist: this.checklistArr
          })
        )
        .then(() => {
          this.toastCtrl
            .create({
              message: "Save as draft successfully!",
              duration: 2000,
              showCloseButton: true
            })
            .present();
          setTimeout(() => {
            loading.dismiss();
          }, 1000);
        });
    };

    const confirm = this.alertCtrl.create({
      title: "Save as Draft?",
      message: "Confirm to save as draft?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            saveDraft();
          }
        }
      ]
    });
    confirm.present();
  }

  saveEntry() {
    if (this.network.type === "none") {
      this.alertCtrl
        .create({
          title: "WARNING!",
          subTitle:
            "No internet connection! Unable to save checklist, please save as draft instead",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    console.log(
      "business_equipment_basic_info: ",
      this.business_equipment_basic_info
    );
    console.log("inspection_info: ", this.inspection_info);
    console.log("checklistArr: ", this.checklistArr);

    const saveEntry = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();

      async.eachSeries(
        this.checklistArr,
        (checklist, callback) => {
          this.inspectionService
            .createInspectionBusinessEquipmentChecklist(
              this.inspection_info.id,
              this.business_equipment_basic_info.business_id,
              this.business_equipment_basic_info.equipment_id,
              {
                equipment_form_id: checklist.equipment_form_id,
                value: checklist.value,
                remarks: checklist.remarks,
                inspection_equipment_id: checklist.inspection_equipment_id
              }
            )
            .then(
              (resp: any) => {
                console.log("resp: ", resp);
                const response = resp.response.data;
                if (response.resp) {
                  if (!_.isEmpty(checklist.uploaded)) {
                    async.eachSeries(
                      checklist.uploaded,
                      (pic, cb) => {
                        this.inspectionAssets
                          .createAllInspectionInputAssets(response.resp.id, {
                            inspection_input_id: response.resp.id,
                            asset_id: pic.id
                          })
                          .then((picresp: any) => {
                            console.log("picresp: ", picresp);
                            cb();
                          });
                      },
                      () => {
                        this.toastCtrl
                          .create({
                            message: resp.response.message,
                            duration: 2000,
                            showCloseButton: true
                          })
                          .present();
                        callback();
                      }
                    );
                  } else {
                    callback();
                  }
                } else {
                  callback();
                }
              },
              error => {
                console.log("error: ", error);
                callback();
              }
            );
        },
        () => {
          // Save
          loading.dismiss();
          this.storage.removeStorage(
            this.storage.storageKey.CHECKLIST_DRAFT + "-" + this.storeKey
          );
          this.callback("save").then(() => {
            this.navCtrl.pop();
          });
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Submit Inspection?",
      message: "Confirm to submit inspection?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            saveEntry();
          }
        }
      ]
    });
    confirm.present();
  }

  ionSlideReachEndEvent(ev: any) {
    this.formBroadcast.emit({
      action: "slide-end",
      params: {}
    });
  }

  ionSlideDidChangeEvent() {
    if (!this.slides.isEnd()) {
      this.formBroadcast.emit({
        action: "slide-next",
        params: {}
      });
    }
  }
}
