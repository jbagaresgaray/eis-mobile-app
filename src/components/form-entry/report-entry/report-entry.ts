import { Component } from '@angular/core';

/**
 * Generated class for the ReportEntryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'report-entry',
  templateUrl: 'report-entry.html'
})
export class ReportEntryComponent {

  text: string;

  constructor() {
    console.log('Hello ReportEntryComponent Component');
    this.text = 'Hello World';
  }

}
