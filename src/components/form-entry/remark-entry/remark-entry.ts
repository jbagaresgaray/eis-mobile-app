import { Component } from "@angular/core";
import { NavController, NavParams, ViewController } from "ionic-angular";
@Component({
  selector: "remark-entry-component",
  templateUrl: "remark-entry.html"
})
export class RemarkEntryComponent {
  params: any = {};

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public viewCtrl: ViewController
  ) {
    this.params = this.navParams.get("params");
    console.log("this.params, ", this.params);
  }

  close() {
    this.viewCtrl.dismiss();
  }

  save() {
    console.log("this.params, ", this.params);
    this.viewCtrl.dismiss("save");
  }
}
