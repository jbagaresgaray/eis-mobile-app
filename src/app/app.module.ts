import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { ZXingScannerModule } from "@zxing/ngx-scanner";
import { RestangularModule } from "ngx-restangular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { JwtModule } from "@auth0/angular-jwt";
import { MultiPickerModule } from "ion-multi-picker";
import { IonicStorageModule } from "@ionic/storage";
import { NgxQRCodeModule } from "ngx-qrcode2";
import { IonicImageViewerModule } from "ionic-img-viewer";

import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { ListPage } from "../pages/list/list";
import { InspectionsPage } from "../pages/inspections/inspections";
import { ReportsPage } from "../pages/reports/reports";
import { BarcodePage } from "../pages/barcode/barcode";
import { LoginPage } from "../pages/login/login";
import { FileuploadPage } from "../pages/fileupload/fileupload";
import { FormContentPage } from "../pages/form-content/form-content";
import { FormEntryPage } from "../pages/form-entry/form-entry";
import { ModalFormPage } from "../pages/modal-form/modal-form";
import { UserProfilePage } from "../pages/user-profile/user-profile";
import { ForgotPage } from "../pages/forgot/forgot";
import { SyncPage } from "../pages/sync/sync";

import { TabsPage } from "../pages/tabs/tabs";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { QRScanner } from "@ionic-native/qr-scanner";
import { Media } from "@ionic-native/media";
import { AppVersion } from "@ionic-native/app-version";
import { Network } from "@ionic-native/network";
import { Camera } from "@ionic-native/camera";
import { FileChooser } from "@ionic-native/file-chooser";
import { File } from "@ionic-native/file";
import { IOSFilePicker } from "@ionic-native/file-picker";

import { ComponentsModule } from "../components/components.module";

import { ScanProvider } from "../providers/scan/scan";
import { AuthProvider } from "../providers/auth/auth";
import { InspectionProvider } from "../providers/inspection/inspection";

import { environment } from "../environments/environment";
import { ChecklistInputProvider } from "../providers/checklist-input/checklist-input";
import { InspectionAssetsProvider } from "../providers/inspection-assets/inspection-assets";
import { InspectionTrackingProvider } from "../providers/inspection-tracking/inspection-tracking";
import { StorageProvider } from "../providers/storage/storage";
import { NetworkProvider } from "../providers/network/network";
import { SyncProvider } from '../providers/sync/sync';

export function RestangularConfigFactory(RestangularProvider) {
  RestangularProvider.setBaseUrl(environment.api_url + environment.api_version);
}

export function tokenGetter() {
  return localStorage.getItem("app.eis.token");
}

const componentsArr: any[] = [
  MyApp,
  HomePage,
  ListPage,
  TabsPage,
  InspectionsPage,
  ReportsPage,
  BarcodePage,
  LoginPage,
  FormContentPage,
  FormEntryPage,
  FileuploadPage,
  ModalFormPage,
  UserProfilePage,
  ForgotPage,
  SyncPage
];

@NgModule({
  declarations: componentsArr,
  imports: [
    BrowserModule,
    ComponentsModule,
    ZXingScannerModule,
    FormsModule,
    ReactiveFormsModule,
    MultiPickerModule,
    NgxQRCodeModule,
    IonicImageViewerModule,
    IonicModule.forRoot(MyApp, {
      mode: "ios",
      backButtonText: "",
      swipeBackEnabled: true,
      scrollAssist: false,
      autoFocusAssist: false
    }),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    }),
    RestangularModule.forRoot(RestangularConfigFactory),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: componentsArr,
  providers: [
    StatusBar,
    SplashScreen,
    QRScanner,
    Media,
    AppVersion,
    Network,
    Camera,
    FileChooser,
    File,
    IOSFilePicker,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ScanProvider,
    AuthProvider,
    InspectionProvider,
    ChecklistInputProvider,
    InspectionAssetsProvider,
    InspectionTrackingProvider,
    StorageProvider,
    NetworkProvider,
    SyncProvider
  ]
})
export class AppModule {}
