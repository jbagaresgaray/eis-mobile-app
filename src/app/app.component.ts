import { Component, ViewChild } from "@angular/core";
import {
  Nav,
  App,
  Platform,
  Events,
  Toast,
  ToastController,
  ActionSheetController
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { AppVersion } from "@ionic-native/app-version";

import { LoginPage } from "../pages/login/login";
import { HomePage } from "../pages/home/home";
import { ListPage } from "../pages/list/list";
import { InspectionsPage } from "../pages/inspections/inspections";
import { ReportsPage } from "./../pages/reports/reports";
import { BarcodePage } from "../pages/barcode/barcode";
import { SyncPage } from "../pages/sync/sync";
import { TabsPage } from "../pages/tabs/tabs";

import { AuthProvider } from "../providers/auth/auth";
import { SyncProvider } from "../providers/sync/sync";
import { NetworkProvider } from "../providers/network/network";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  globalToast: Toast;

  isAuthenticated: boolean;

  rootPage: any = LoginPage;
  _appVersion: any = {};
  pages: Array<{ title: string; component: any; icon: string }>;

  constructor(
    public app: App,
    public platform: Platform,
    public events: Events,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public appVersion: AppVersion,
    public network: NetworkProvider,
    public auth: AuthProvider,
    public sync: SyncProvider
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: "Dashboard", component: TabsPage, icon: "home" },
      {
        title: "Inspections",
        component: InspectionsPage,
        icon: "clipboard-text"
      },
      { title: "Reports", component: ReportsPage, icon: "poll-box" },
      {
        title: "Equipment QR Scan",
        component: BarcodePage,
        icon: "qrcode-scan"
      }
    ];

    this.events.subscribe("user:login", () => {
      this.isAuthenticated = this.auth.isAuthenticated();
    });

    this.events.subscribe("sync:data", () => {
      this.downloadDataFromCloud();
    });

    this.events.subscribe("sync:data:current_user", () => {
      console.log("sync:data:current_user");
      this.sync.downloadCurrentUser();
    });

    this.events.subscribe("sync:data:inspection", () => {
      console.log("sync:data:inspection");
      this.sync.downloadInspections();
    });

    this.events.subscribe("sync:data:inspection_detail", inspectionId => {
      console.log("sync:data:inspection_detail: ", inspectionId);
      this.sync.downloadInspectionDetail(inspectionId);
    });

    this.events.publish(
      "sync:data:inspection_checklist",
      (inspectionId, businessId, equipmentId) => {
        console.log("sync:data:inspection_checklist");
        this.sync.downloadInspectionEquipment(inspectionId, businessId, equipmentId);
        this.sync.downloadInspectionEquipmentChecklist(
          inspectionId,
          businessId,
          equipmentId
        );
      }
    );
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.network.initializeNetworkEvents();
      this.network.getNetworkStatus().subscribe(data => {
        console.log("platform ready", data);
        console.log(
          "this.network.getNetworkType(): ",
          this.network.getNetworkType()
        );
        if (data == 0) {
          this.auth.isOnline = true;
          this.events.publish("app:online");

          /* this.globalToast = this.toastCtrl.create({
            message: "Online!",
            position: "top",
            duration: 5000,
            cssClass: "offline-toast-notif-success"
          });
          this.globalToast.present(); */
        } else {
          this.auth.isOnline = false;
          this.events.publish("app:offline");
          /* this.globalToast = this.toastCtrl.create({
            message: "No Internet Connection!",
            position: "top",
            duration: 5000,
            cssClass: "offline-toast-notif-danger"
          });
          this.globalToast.present(); */
        }
      });

      if (this.platform.is("cordova")) {
        this.appVersion.getAppName().then((appName: string) => {
          console.log("appName: ", appName);
          this._appVersion.appName = appName;
        });
        this.appVersion.getPackageName().then((packageName: string) => {
          console.log("packageName: ", packageName);
          this._appVersion.packageName = packageName;
        });
        this.appVersion.getVersionCode().then((versionCode: string) => {
          console.log("versionCode: ", versionCode);
          this._appVersion.versionCode = versionCode;
        });
        this.appVersion.getVersionNumber().then((versionNumber: string) => {
          console.log("versionNumber: ", versionNumber);
          this._appVersion.versionNumber = versionNumber;
        });
      }

      this.isAuthenticated = this.auth.isAuthenticated();

      if (this.auth.getUserToken()) {
        if (this.auth.isAuthenticated()) {
          this.rootPage = SyncPage;
        } else {
          this.rootPage = LoginPage;
        }
      } else {
        this.rootPage = LoginPage;
      }

      if (this.platform.is("tablet")) {
        this.auth.setSplitPane(true);
      } else {
        this.auth.setSplitPane(false);
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logOutApp() {
    const logoutApp = () => {
      this.auth.logoutApp();
      setTimeout(() => {
        this.isAuthenticated = this.auth.isAuthenticated();
        this.app.getRootNav().setRoot(LoginPage, null, {
          animate: true,
          direction: "back"
        });
      }, 300);
    };

    const actionSheet = this.actionSheetCtrl.create({
      title: "Your profile will be saved. Can't wait to have you back",
      buttons: [
        {
          text: "Logout",
          role: "destructive",
          handler: () => {
            logoutApp();
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  // ====================================================================================
  // ====================================================================================
  // ====================================================================================
  // ====================================================================================

  private downloadDataFromCloud() {
    console.log("syncDataFromCloud");

    this.sync.downloadCurrentUser();
    this.sync.downloadAllData();
  }
}
