import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  Loading,
  Alert,
  Events
} from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as _ from "lodash";
import { LoginPage } from "../login/login";

@Component({
  selector: "page-forgot",
  templateUrl: "forgot.html"
})
export class ForgotPage {
  loginForm: FormGroup;
  submitted = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      email: ["", Validators.required]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ForgotPage");
  }

  forgotPassword() {}

  loginApp() {
    this.navCtrl.setRoot(LoginPage, null, { animate: true, direction: "back" });
  }
}
