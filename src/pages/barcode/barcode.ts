import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  Platform,
  ToastController,
  Toast,
  Events
} from "ionic-angular";
import { ZXingScannerComponent } from "@zxing/ngx-scanner";
import { Media, MediaObject } from "@ionic-native/media";

import { FormContentPage } from "../form-content/form-content";

import { ScanProvider } from "../../providers/scan/scan";
import { StorageProvider } from "../../providers/storage/storage";

@Component({
  selector: "page-barcode",
  templateUrl: "barcode.html"
})
export class BarcodePage {
  @ViewChild("scanner")
  scanner: ZXingScannerComponent;

  hasCameras = false;
  hasPermission: boolean;
  qrResultString: string;

  availableDevices: MediaDeviceInfo[];
  selectedDevice: MediaDeviceInfo;

  public browserScanEnabled: boolean;
  private scannerIsAvailable: boolean;
  private scannerHasPermission: boolean;
  private scannerIsDenied: boolean;
  private scannerIsRestricted: boolean;
  public canEnableLight: boolean;
  public canChangeCamera: boolean;
  public lightActive: boolean;
  public cameraToggleActive: boolean;
  public scannerStates;
  public canOpenSettings: boolean;
  public currentState: string;
  public tabBarElement;
  public isCordova: boolean;
  public isCameraSelected: boolean;
  public fromAddressbook: boolean;
  public fromImport: boolean;
  public fromJoin: boolean;
  public fromSend: boolean;

  isOnline: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public events: Events,
    public toastController: ToastController,
    private scanProvider: ScanProvider,
    private storage: StorageProvider,
    private media: Media
  ) {
    this.isCameraSelected = false;
    this.browserScanEnabled = false;
    this.canEnableLight = true;
    this.canChangeCamera = true;
    this.scannerStates = {
      unauthorized: "unauthorized",
      denied: "denied",
      unavailable: "unavailable",
      loading: "loading",
      visible: "visible"
    };
    this.scannerIsAvailable = true;
    this.scannerHasPermission = false;
    this.scannerIsDenied = false;
    this.scannerIsRestricted = false;
    this.canOpenSettings = false;
    this.isCordova = this.platform.is("cordova") ? true : false;

    this.isOnline = true;

    this.events.subscribe("app:online", () => {
      this.isOnline = true;
    });
    this.events.subscribe("app:offline", () => {
      this.isOnline = false;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad BarcodePage");
  }

  ionViewWillLeave() {
    this.events.unsubscribe("incomingDataError");
    this.events.unsubscribe("finishIncomingDataMenuEvent");
    this.events.unsubscribe("scannerServiceInitialized");
    if (!this.isCordova) {
      this.scanner.resetCodeReader();
    } else {
      this.cameraToggleActive = false;
      this.lightActive = false;
      this.scanProvider.frontCameraEnabled = false;
      this.scanProvider.deactivate();
    }
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter BarcodePage");
    if (!this.isCordova) {
      if (!this.isCameraSelected) {
        this.loadCamera();
      } else {
        this.scanner.startScan(this.selectedDevice);
      }
    } else {
      // try initializing and refreshing status any time the view is entered
      if (this.scannerHasPermission) {
        this.activate();
      } else {
        if (!this.scanProvider.isInitialized()) {
          this.scanProvider.gentleInitialize().then(() => {
            this.authorize();
          });
        } else {
          this.authorize();
        }
      }
      this.events.subscribe("scannerServiceInitialized", () => {
        console.debug(
          "Scanner initialization finished, reinitializing scan view..."
        );
        this._refreshScanView();
      });
    }
  }

  private playBeep() {
    const file: MediaObject = this.media.create("./assets/beep.mp3");
    console.log("file: ", file);
    file.play();
  }

  testScanner(params: any) {
    this.navCtrl.push(FormContentPage, {
      action: "report_details_scan",
      params: params
    });
  }

  public loadCamera() {
    this.scanner.camerasFound.subscribe((devices: MediaDeviceInfo[]) => {
      this.hasCameras = true;
      this.availableDevices = devices;
      this.onDeviceSelectChange();
    });

    this.scanner.camerasNotFound.subscribe(() => {
      console.error(
        "An error has occurred when trying to enumerate your video-stream-enabled devices."
      );
    });
    this.scanner.askForPermission().then((answer: boolean) => {
      this.hasPermission = answer;
    });
  }

  public activate(): void {
    this.scanProvider
      .activate()
      .then(() => {
        console.info("Scanner activated, setting to visible...");
        this.updateCapabilities();
        this.handleCapabilities();
        this.currentState = this.scannerStates.visible;

        // resume preview if paused
        this.scanProvider.resumePreview();

        this.scanProvider.scan().then((contents: string) => {
          if (this.platform.is("cordova")) {
            this.playBeep();
          }
          this.scanProvider.pausePreview();
          this.handleSuccessfulScan(contents);
        });
      })
      .catch(err => {
        console.error(err);
      });
  }

  private handleSuccessfulScan(contents: any): void {
    console.log("handleSuccessfulScan: ", contents);
    // this.storage.setStorage(this.storage.storageKey.SCANNED_INSPECTION, contents);
    this.testScanner(contents);
  }

  private updateCapabilities(): void {
    let capabilities = this.scanProvider.getCapabilities();
    this.scannerIsAvailable = capabilities.isAvailable;
    this.scannerHasPermission = capabilities.hasPermission;
    this.scannerIsDenied = capabilities.isDenied;
    this.scannerIsRestricted = capabilities.isRestricted;
    this.canEnableLight = capabilities.canEnableLight;
    this.canChangeCamera = capabilities.canChangeCamera;
    this.canOpenSettings = capabilities.canOpenSettings;
  }

  private handleCapabilities(): void {
    // always update the view
    if (!this.scanProvider.isInitialized()) {
      this.currentState = this.scannerStates.loading;
    } else if (!this.scannerIsAvailable) {
      this.currentState = this.scannerStates.unavailable;
    } else if (this.scannerIsDenied) {
      this.currentState = this.scannerStates.denied;
    } else if (this.scannerIsRestricted) {
      this.currentState = this.scannerStates.denied;
    } else if (!this.scannerHasPermission) {
      this.currentState = this.scannerStates.unauthorized;
    }
    console.debug("Scan view state set to: " + this.currentState);
  }

  private _refreshScanView(): void {
    this.updateCapabilities();
    this.handleCapabilities();
    if (this.scannerHasPermission) {
      this.activate();
    }
  }

  public authorize(): void {
    this.scanProvider.initialize().then(() => {
      this._refreshScanView();
    });
  }

  public attemptToReactivate(): void {
    this.scanProvider.reinitialize();
  }

  public openSettings(): void {
    this.scanProvider.openSettings();
  }

  public toggleLight(): void {
    this.scanProvider
      .toggleLight()
      .then(resp => {
        this.lightActive = resp;
      })
      .catch(error => {
        console.warn("scanner error: " + error);
      });
  }

  public toggleCamera(): void {
    this.scanProvider
      .toggleCamera()
      .then(resp => {
        this.cameraToggleActive = resp;
        this.lightActive = false;
      })
      .catch(error => {
        console.warn("scanner error: " + error);
      });
  }

  handleQrCodeResult(resultString: string) {
    this.scanner.resetCodeReader();
    setTimeout(() => {
      this.handleSuccessfulScan(resultString);
    }, 0);
  }

  onDeviceSelectChange() {
    if (!this.isCameraSelected) {
      for (const device of this.availableDevices) {
        if (device.kind == "videoinput") {
          this.selectedDevice = this.scanner.getDeviceById(device.deviceId);
          this.isCameraSelected = true;
          break;
        }
      }
    }
  }
}
