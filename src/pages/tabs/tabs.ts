import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";

import { HomePage } from "../home/home";
import { InspectionsPage } from "../inspections/inspections";
import { ReportsPage } from "../reports/reports";
import { BarcodePage } from "../barcode/barcode";
import { UserProfilePage } from "../user-profile/user-profile";

import { AuthProvider } from "../../providers/auth/auth";

@Component({
  selector: "page-tabs",
  templateUrl: "tabs.html"
})
export class TabsPage implements OnInit {
  homeRoot = HomePage;
  inspectionsRoot = InspectionsPage;
  reportsRoot = ReportsPage;
  barcodeRoot = BarcodePage;
  profileRoot = UserProfilePage;

  constructor(
    public navCtrl: NavController,
    private authService: AuthProvider
  ) {}

  ngOnInit() {}
}
