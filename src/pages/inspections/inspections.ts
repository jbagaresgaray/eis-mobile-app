import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  Events,
  ActionSheetController,
  AlertController,
  LoadingController
} from "ionic-angular";
import * as _ from "lodash";
import * as moment from "moment";

import { FormContentPage } from "../form-content/form-content";
import { StorageProvider } from "../../providers/storage/storage";
import { InspectionProvider } from "../../providers/inspection/inspection";

@Component({
  selector: "page-inspections",
  templateUrl: "inspections.html"
})
export class InspectionsPage {
  inspectionsArr: any[] = [];
  fakeArr: any[] = [];

  showContent: boolean;
  isOnline: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private storage: StorageProvider,
    private inspectionService: InspectionProvider
  ) {
    for (let index = 0; index < 20; index++) {
      this.fakeArr.push(index);
    }

    this.isOnline = true;

    this.events.subscribe("app:online",()=>{
      this.isOnline = true;
    });
    this.events.subscribe("app:offline",()=>{
      this.isOnline = false;
    });
  }

  ionViewDidLoad() {
    this.initData();
  }

  private initData(ev?: any) {
    this.showContent = false;

    if (ev) {
      this.events.publish("sync:data:inspection");
    }

    this.events.subscribe("sync:data:inspection:complete", () => {
      getInspections();

      if (ev) {
        this.showContent = true;
        ev.complete();
      }
    });

    const getInspections = () => {
      this.storage
        .getStorage(this.storage.storageKey.INSPECTIONS)
        .then((inspections: any) => {
          this.inspectionsArr = inspections;

          _.each(this.inspectionsArr, (row: any) => {
            row.inspection_date_formatted = moment(row.inspection_date).format(
              "MMM DD"
            );

            if (row.request_status === "In progress") {
              row.color = null;
            } else if (row.request_status === "Pending Inspection") {
              row.color = "j3lightwarning";
            } else if (row.request_status === "Inspection Finished") {
              row.color = null;
            }
          });
          this.showContent = true;
        });
    };

    getInspections();
  }

  doRefresh(ev: any) {
    this.initData(ev);
  }

  viewDetails(inspection: any, event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.navCtrl.push(FormContentPage, {
      action: "inspection_details",
      inspection: inspection
    });
  }

  presentActionSheet(inspection: any, event: any) {
    event.preventDefault();
    event.stopPropagation();

    const deleteInspection = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.inspectionService.deleteInspectionInfo(inspection.id).then(
        (data: any) => {
          if (data) {
            this.alertCtrl
              .create({
                title: "SUCCESS",
                subTitle: "Inspection successfully deleted!",
                buttons: ["OK"]
              })
              .present();
            this.initData(null);
          }
          loading.dismiss();
        },
        error => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
    };

    const actionSheet = this.actionSheetCtrl.create({
      title: "Choose action for this inspection",
      buttons: [
        {
          text: "Delete",
          role: "destructive",
          handler: () => {
            const confirm = this.alertCtrl.create({
              title: "Delete this inspection?",
              message: "Confirm to delete this inspection?",
              buttons: [
                {
                  text: "Cancel",
                  handler: () => {}
                },
                {
                  text: "Confirm",
                  handler: () => {
                    deleteInspection();
                  }
                }
              ]
            });
            confirm.present();
          }
        },
        {
          text: "Update",
          handler: () => {
            console.log("Archive clicked");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
}
