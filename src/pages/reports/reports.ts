import { Component } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";

import { FormEntryPage } from "../../pages/form-entry/form-entry";

@Component({
  selector: "page-reports",
  templateUrl: "reports.html"
})
export class ReportsPage {
  isOnline: boolean;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.isOnline = true;

    this.events.subscribe("app:online", () => {
      this.isOnline = true;
    });
    this.events.subscribe("app:offline", () => {
      this.isOnline = false;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ReportsPage");
  }

  viewDetails() {
    this.navCtrl.push(FormEntryPage, {
      action: "report_details_entry"
    });
  }
}
