import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ActionSheetController,
  App,
  Events,
  AlertController
} from "ionic-angular";

import { LoginPage } from "./../login/login";
import { SyncPage } from "./../sync/sync";

import { AuthProvider } from "./../../providers/auth/auth";
import { StorageProvider } from "../../providers/storage/storage";

@Component({
  selector: "page-user-profile",
  templateUrl: "user-profile.html"
})
export class UserProfilePage {
  user: any = {};
  isOnline: boolean;

  constructor(
    public app: App,
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public auth: AuthProvider,
    private _storage: StorageProvider
  ) {
    this.isOnline = true;

    this.events.subscribe("app:online", () => {
      this.isOnline = true;
    });
    this.events.subscribe("app:offline", () => {
      this.isOnline = false;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad UserProfilePage");
    this.user =
      JSON.parse(localStorage.getItem(this._storage.storageKey.CURRENT_USER)) ||
      {};
    console.log("currentUser: ", this.user);
  }

  update() {}

  sync() {
    const confirm = this.alertCtrl.create({
      title: "Sync Data?",
      message: "Confirm to sync data from cloud?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            this._storage.cleanStorage().then(() => {
              // this.events.publish("sync:data");
              this.app.getRootNav().setRoot(SyncPage, null, {
                animate: true,
                direction: "back"
              });
            });
          }
        }
      ]
    });
    confirm.present();
  }

  signOut() {
    const logoutApp = () => {
      this.auth.logoutApp();
      setTimeout(() => {
        this.app.getRootNav().setRoot(LoginPage, null, {
          animate: true,
          direction: "back"
        });
        /* this.navCtrl.setRoot(LoginPage, null, {
          animate: true,
          direction: "back"
        }); */
      }, 300);
    };

    const actionSheet = this.actionSheetCtrl.create({
      title: "Your profile will be saved. Can't wait to have you back",
      buttons: [
        {
          text: "Logout",
          role: "destructive",
          handler: () => {
            logoutApp();
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
}
