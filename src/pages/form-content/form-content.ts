import { Component, EventEmitter } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";
import * as _ from "lodash";

@Component({
  selector: "page-form-content",
  templateUrl: "form-content.html"
})
export class FormContentPage {
  title: string;
  subtitle: string;
  action: string;

  inspection: any = {};
  isOnline: boolean;

  refreshNotification: EventEmitter<any> = new EventEmitter();
  viewWillEnterEvent: EventEmitter<any> = new EventEmitter();

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.action = navParams.get("action");

    if (this.action == "inspection_details") {
      this.inspection = navParams.get("inspection");
      if (!_.isEmpty(this.inspection)) {
        this.title = this.inspection.business_name;
      }
    } else if (this.action == "report_details") {
      this.title = "Reports";
      this.subtitle = "ACME X: Bonggo";
    }

    this.isOnline = true;

    this.events.subscribe("app:online", () => {
      this.isOnline = true;
    });
    this.events.subscribe("app:offline", () => {
      this.isOnline = false;
    });
  }

  ionViewDidLoad() {}

  ionViewWillEnter() {
    console.log("ionViewWillEnter");
    this.viewWillEnterEvent.emit();
  }

  doRefresh(ev) {
    this.refreshNotification.emit(ev);
  }
}
