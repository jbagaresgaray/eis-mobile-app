import { Component, NgZone } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import {
  NavController,
  NavParams,
  ViewController,
  ActionSheetController,
  AlertController,
  Platform,
  ToastController,
  normalizeURL
} from "ionic-angular";
import * as _ from "lodash";

import { Camera, CameraOptions } from "@ionic-native/camera";
import { FileChooser } from "@ionic-native/file-chooser";
import { File, FileEntry, IFile } from "@ionic-native/file";
import { IOSFilePicker } from "@ionic-native/file-picker";

import { environment } from "../../environments/environment";

import { AuthProvider } from "../../providers/auth/auth";

@Component({
  selector: "page-fileupload",
  templateUrl: "fileupload.html"
})
export class FileuploadPage {
  public isUploading = false;
  public isImageAdded = false;
  public uploadingProgress = {};
  public uploadingHandler = {};

  inspection: any = {};
  checklist: any = {};

  public images: any = [];
  public uploadedFiles: any[] = [];

  public serverUrl = environment.api_url + environment.api_version + "assets";

  public filesToUpload: Array<File> = [];
  protected imagesValue: Array<any>;

  constructor(
    public sanitization: DomSanitizer,
    public zone: NgZone,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public fileChooser: FileChooser,
    public camera: Camera,
    public file: File,
    public filePicker: IOSFilePicker,
    public authService: AuthProvider
  ) {
    this.inspection = navParams.get("inspection");
    this.checklist = navParams.get("checklist");

    console.log("inspection: ", this.inspection);
    console.log("checklist: ", this.checklist);
  }

  public uploadImages(): Promise<Array<any>> {
    return new Promise((resolve, reject) => {
      this.isUploading = true;
      Promise.all(
        this.filesToUpload.map(image => {
          return this.uploadImage(image);
        })
      )
        .then((upload: any) => {
          console.log("upload: ", upload);
          resolve();

          _.each(upload, (rows: any) => {
            rows.response.public_path_url = this.sanitization.bypassSecurityTrustStyle(
              "url(" + rows.response.public_path + ")"
            );
            this.uploadedFiles.push(rows.response);
          });

          this.isUploading = false;
          const alert = this.alertCtrl.create({
            title: "SUCCESS",
            subTitle: "Image uploaded!",
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.viewCtrl.dismiss({
              action: "save",
              uploaded: this.uploadedFiles
            });
          });
          alert.present();
        })
        .catch(reason => {
          this.isUploading = false;
          this.uploadingProgress = {};
          this.uploadingHandler = {};
          reject(reason);
        });
    });
  }

  public abort() {
    if (!this.isUploading) return;
    this.isUploading = false;
    for (let key in this.uploadingHandler) {
      this.uploadingHandler[key].abort();
    }
  }

  // ======================================================================
  protected removeImage(image) {
    if (this.isUploading) return;
    this.confirm("Are you sure to remove it?").then(value => {
      if (value) {
        this.removeFromArray(this.imagesValue, image);
        this.removeFromArray(this.images, image.url);
      }
    });
  }

  protected showAddImage() {
    const uuid = parseInt(new Date().getTime().toString());
    if (!this.platform.is("cordova")) {
      let input = document.createElement("input");
      input.type = "file";
      input.accept = "image/x-png,image/gif,image/jpeg";
      input.click();
      input.onchange = () => {
        const blob = window.URL.createObjectURL(input.files[0]);
        const blob2: any = input.files[0];
        blob2.uuid = uuid;
        this.images.push({
          url: blob,
          uuid: uuid
        });
        this.filesToUpload.push(blob2);
        this.trustImages();
      };
    } else {
      new Promise((resolve, reject) => {
        const actionSheet = this.actionSheetCtrl.create({
          title: "Add a photo",
          buttons: [
            {
              text: "From photo library",
              handler: () => {
                resolve(this.camera.PictureSourceType.PHOTOLIBRARY);
              }
            },
            {
              text: "From camera",
              handler: () => {
                resolve(this.camera.PictureSourceType.CAMERA);
              }
            },
            {
              text: "Cancel",
              role: "cancel",
              handler: () => {
                reject();
              }
            }
          ]
        });
        actionSheet.present();
      })
        .then(sourceType => {
          if (!this.platform.is("cordova")) {
            return;
          }

          const options: CameraOptions = {
            quality: 100,
            sourceType: sourceType as number,
            saveToPhotoAlbum: false,
            correctOrientation: true
          };
          this.camera.getPicture(options).then(imagePath => {
            this.file.resolveLocalFilesystemUrl(imagePath).then(
              (entry: FileEntry) => {
                entry.file(
                  (meta: IFile) => {
                    console.log("file: ", meta);
                    console.log("this.platform: ", this.platform);

                    if (this.platform.is("ios")) {
                      this.images.push({
                        url: normalizeURL(imagePath),
                        file: meta,
                        size: meta.size,
                        name: meta.name,
                        uuid: uuid
                      });
                    } else {
                      this.images.push({
                        url: imagePath,
                        file: meta,
                        size: meta.size,
                        name: meta.name,
                        uuid: uuid
                      });
                    }
                    console.log("this.images: ", this.images);
                    this.filesToUpload.push(imagePath);
                    this.trustImages();
                  },
                  error => {
                    console.error("error getting fileentry file!", error);
                  }
                );
              },
              error => {
                console.error("error getting file! ", error);
              }
            );
          });
        })
        .catch(() => {});
    }
  }

  private uploadImage(targetPath) {
    console.log("targetPath: ", targetPath);
    return new Promise((resolve, reject) => {
      this.uploadingProgress[targetPath] = 0;
      const formData: FormData = new FormData();
      const xhr2: XMLHttpRequest = new XMLHttpRequest();
      const vm = this;

      if (this.platform.is("cordova")) {
        vm.uploadingProgress[targetPath.uuid] = 0;
        vm.file.resolveLocalFilesystemUrl(targetPath).then(
          (entry: FileEntry) => {
            entry.file(
              (meta: IFile) => {
                // formData.append("acl", vm.acl);
                var reader = new FileReader();
                reader.onloadend = function(evt: any) {
                  var blob = new Blob([new Uint8Array(evt.target.result)], {
                    type: meta.type
                  });
                  console.log("blob: ", blob);
                  formData.append("file", blob);
                  vm.uploadingHandler[targetPath.uuid] = xhr2;
                  xhr2.timeout = 900000;
                  xhr2.onreadystatechange = () => {
                    if (xhr2.readyState === 4) {
                      if (xhr2.status === 200) {
                        resolve(xhr2.response);
                      } else {
                        askRetry();
                      }
                    }
                  };

                  xhr2.upload.onprogress = event => {
                    const progress = (event.loaded * 100) / event.total;
                    vm.zone.run(() => {
                      console.log("progress: ", progress);
                      vm.uploadingProgress[targetPath.uuid] = progress;
                    });
                  };
                  xhr2.open("POST", vm.serverUrl, true);
                  xhr2.setRequestHeader(
                    "Authorization",
                    "Bearer " + vm.authService.getUserToken()
                  );
                  xhr2.send(formData);
                };
                reader.readAsArrayBuffer(meta);
              },
              error => {
                console.error("error getting fileentry file!", error);
              }
            );
          },
          error => {
            console.error("error getting file! ", error);
          }
        );
      } else {
        const formData: FormData = new FormData();
        const xhr2: XMLHttpRequest = new XMLHttpRequest();
        formData.append("file", targetPath);
        this.uploadingHandler[targetPath] = xhr2;
        xhr2.onreadystatechange = () => {
          if (xhr2.readyState === 4) {
            if (xhr2.status === 200) resolve(JSON.parse(xhr2.response));
            else askRetry();
          }
        };

        xhr2.upload.onprogress = event => {
          this.uploadingProgress[targetPath] =
            (event.loaded * 100) / event.total;
        };

        xhr2.open("POST", this.serverUrl, true);
        xhr2.setRequestHeader(
          "Authorization",
          "Bearer " + this.authService.getUserToken()
        );
        xhr2.send(formData);
      }

      let askRetry = () => {
        // might have been aborted
        if (!this.isUploading) return reject(null);
        this.confirm("Do you wish to retry?", "Upload failed").then(res => {
          if (!res) {
            this.isUploading = false;
            for (let key in this.uploadingHandler) {
              this.uploadingHandler[key].abort();
            }
            return reject(null);
          } else {
            if (!this.isUploading) return reject(null);
            this.uploadImage(targetPath).then(resolve, reject);
          }
        });
      };
    });
  }

  private removeFromArray<T>(array: Array<T>, item: T) {
    let index: number = array.indexOf(item);
    if (index !== -1) {
      array.splice(index, 1);
    }
  }

  private confirm(text, title = "", yes = "Yes", no = "No") {
    return new Promise(resolve => {
      this.alertCtrl
        .create({
          title: title,
          message: text,
          buttons: [
            {
              text: no,
              role: "cancel",
              handler: () => {
                resolve(false);
              }
            },
            {
              text: yes,
              handler: () => {
                resolve(true);
              }
            }
          ]
        })
        .present();
    });
  }
  private trustImages() {
    /* _this.imagesValue = _this.images.map(val => {
      return {
        url: val.url,
        uuid: val.uuid,
        sanitized: _this.sanitization.bypassSecurityTrustStyle(
          "url(" + val.url + ")"
        )
      };
    }); */
    this.zone.run(() => {
      this.imagesValue = this.images.map(val => {
        return {
          url: val.url,
          uuid: val.uuid,
          sanitized: this.sanitization.bypassSecurityTrustStyle(
            "url(" + val.url + ")"
          )
        };
      });
      console.log("this.imagesValue: ", this.imagesValue);
    });
  }

  private showToast(text: string) {
    this.toastCtrl
      .create({
        message: text,
        duration: 5000,
        position: "bottom"
      })
      .present();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad FileuploadPage");
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  submitEntry() {
    const confirm = this.alertCtrl.create({
      title: "Upload File?",
      message: "Confirm to upload asset?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            this.uploadImages().then(() => {});
          }
        }
      ]
    });
    confirm.present();
  }
}
