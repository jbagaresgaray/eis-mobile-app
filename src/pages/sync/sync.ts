import { Component } from "@angular/core";
import { NavController, NavParams, Platform, Events } from "ionic-angular";
import * as async from "async";
import * as _ from "lodash";

import { AuthProvider } from "../../providers/auth/auth";
import { StorageProvider } from "../../providers/storage/storage";
import { InspectionProvider } from "../../providers/inspection/inspection";
import { SyncProvider } from "../../providers/sync/sync";
import { NetworkProvider } from "../../providers/network/network";

import { TabsPage } from "../tabs/tabs";
import { HomePage } from "../home/home";

@Component({
  selector: "page-sync",
  templateUrl: "sync.html"
})
export class SyncPage {
  timer = 0;
  private _incTimeout: any;

  constructor(
    public platform: Platform,
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public _storage: StorageProvider,
    public inspectionService: InspectionProvider,
    public network: NetworkProvider,
    public sync: SyncProvider
  ) {
    this.events.subscribe("sync:data:complete", () => {
      console.log("sync:data:complete");
      this.stop();
    });
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      if (this.platform.is("cordova")) {
        this.network.getNetworkStatus().subscribe(data => {
          console.log("getNetworkStatus", data);
          if (data === 0) {
            if (this.timer === 0) {
              this.setTimer(this.timer > 0 ? this.timer : 2);
              setTimeout(() => {
                this.stop();
              }, 2000);
            }
          } else {
            if (this.timer === 0) {
              this.setTimer(this.timer > 0 ? this.timer : 2);
            }
            this.downloadDataFromCloud();
          }
        });
      } else {
        if (this.timer === 0) {
          this.setTimer(this.timer > 0 ? this.timer : 2);
        }
        this.downloadDataFromCloud();
      }
    });
  }

  private downloadDataFromCloud() {
    console.log("syncDataFromCloud");

    this.sync.downloadCurrentUser();
    this.sync.downloadAllData();
  }

  private setTimer(n) {
    if (n === 0) {
      n = 2;
    }
    this.timer = n;
    clearTimeout(this._incTimeout);
    if (this.timer > 0 && this.timer < 100) {
      this._incTimeout = setTimeout(() => this.increment(), 250);
    }
  }

  stop() {
    this.complete();

    if (this.platform.is("tablet")) {
      this.navCtrl.setRoot(HomePage);
    } else {
      this.navCtrl.setRoot(TabsPage);
    }
  }

  complete() {
    if (this.timer === 0) {
      return;
    }

    if (this.timer > 0) {
      if (this.timer !== 100) {
        this.setTimer(100);
      }
      setTimeout(() => this.setTimer(0), 500);
    }
  }

  private increment(rnd = 0) {
    if (rnd > 0) {
      this.setTimer(this.timer + rnd);
    }

    const stat = this.timer;
    if (stat >= 0 && stat < 25) {
      // Start out between 3 - 6% increments
      rnd = Math.random() * (5 - 3 + 1) + 3;
    } else if (stat >= 25 && stat < 65) {
      // increment between 0 - 3%
      rnd = Math.random() * 3;
    } else if (stat >= 65 && stat < 90) {
      // increment between 0 - 2%
      rnd = Math.random() * 2;
    } else if (stat >= 90 && stat < 99) {
      // finally, increment it .5 %
      rnd = 0.5;
    } else {
      // after 99%, don't increment:
      rnd = 0;
    }

    this.setTimer(this.timer + rnd);
  }
}
