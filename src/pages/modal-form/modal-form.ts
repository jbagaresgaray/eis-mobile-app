import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

@Component({
  selector: "page-modal-form",
  templateUrl: "modal-form.html"
})
export class ModalFormPage {
  action: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.action = this.navParams.get("action");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ModalFormPage");
  }
}
