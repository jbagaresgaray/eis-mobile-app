import { Component, EventEmitter } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";
import * as _ from "lodash";

@Component({
  selector: "page-form-entry",
  templateUrl: "form-entry.html"
})
export class FormEntryPage {
  title: string;
  subtitle: string;
  action: string;

  equipment: any = {};
  inspection: any = {};

  showSubmit: boolean;
  hasWarnings: boolean;
  showLoading: boolean;
  viewCanLeave: boolean;

  isOnline: boolean;

  createNotification: EventEmitter<any> = new EventEmitter();
  saveNotification: EventEmitter<any> = new EventEmitter();
  viewWillLeaveNotification: EventEmitter<any> = new EventEmitter();
  _slideNext: EventEmitter<any> = new EventEmitter();
  _slidePrev: EventEmitter<any> = new EventEmitter();

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.action = navParams.get("action");

    if (this.action == "inspection_details_entry") {
      this.inspection = navParams.get("inspection");
      this.equipment = navParams.get("equipment");

      if (!_.isEmpty(this.inspection)) {
        this.title = this.inspection.business_name;
        this.subtitle = this.equipment.equipment_name;
      }
    } else if (this.action == "report_details_entry") {
      this.title = "Reports";
      this.subtitle = "ACME X: Bonggo";
    }

    this.viewCanLeave = true;

    this.isOnline = true;

    this.events.subscribe("app:online", () => {
      this.isOnline = true;
    });
    this.events.subscribe("app:offline", () => {
      this.isOnline = false;
    });
  }

  ionViewDidLoad() {
    this.showSubmit = false;
    this.hasWarnings = false;
    this.showLoading = true;
  }

  ionViewWillLeave() {
    console.log("ionViewWillLeave");
    this.viewWillLeaveNotification.emit();
  }

  viewDidLeave(value) {
    console.log("viewDidLeave: ", value);
    this.viewCanLeave = value;
  }

  ionViewCanLeave(): boolean {
    // here we can either return true or false
    // depending on if we want to leave this view
    if (this.viewCanLeave) {
      return true;
    } else {
      return false;
    }
  }

  componentValidations(ev: any) {
    if (ev && ev.action == "slide-end") {
      this.showSubmit = true;
      this.hasWarnings = false;
    } else if (ev && ev.action == "slide-next") {
      this.showSubmit = false;
      this.hasWarnings = false;
    } else if (ev && ev.action == "form-loading-start") {
      this.showLoading = true;
    } else if (ev && ev.action == "form-loading-end") {
      this.showLoading = false;
    }
  }

  slideNext() {
    this._slideNext.emit();
  }

  slidePrev() {
    this._slidePrev.emit();
  }

  saveLocally() {
    this.saveNotification.emit();
  }

  saveEntry() {
    this.createNotification.emit();
  }
}
