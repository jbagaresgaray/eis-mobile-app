import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  Platform,
  Events,
  ToastController
} from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as _ from "lodash";

import { TabsPage } from "../tabs/tabs";
import { SyncPage } from "../sync/sync";
import { HomePage } from "../home/home";
import { AuthProvider } from "../../providers/auth/auth";
import { ForgotPage } from "../forgot/forgot";
import { StorageProvider } from "../../providers/storage/storage";

@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  loginForm: FormGroup;
  submitted = false;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder,
    private authService: AuthProvider,
    private _storage: StorageProvider
  ) {
    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
  }

  loginApp() {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();

    this.submitted = true;

    if (this.loginForm.invalid) {
      loading.dismiss();
      return;
    }

    this.authService.authenticate(this.loginForm.value).then(
      (data: any) => {
        console.log("data: ", data);
        if (data) {
          const resp = data.response;
          this.authService.saveUserToken(resp.token);
          this.authService.saveUserInfo(resp.user);
          this.events.publish("user:login");
          this.events.publish("sync:data");

          this.navCtrl.setRoot(SyncPage);
        }
        this.submitted = false;
      },
      error => {
        this.submitted = false;
        loading.dismiss();
        if (error) {
          console.log("error: ", error.error);
          // this.toastr.error(error.msg, "WARNING");
          this.toastCtrl
            .create({
              message: error.error.msg,
              duration: 2000
            })
            .present();
        }
      }
    );
  }

  forgotPassword() {
    this.navCtrl.push(ForgotPage);
  }
}
