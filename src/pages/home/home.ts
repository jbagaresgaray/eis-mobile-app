import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  Events,
  ActionSheetController,
  AlertController,
  LoadingController
} from "ionic-angular";
import * as _ from "lodash";
import * as moment from "moment";

import { FormContentPage } from "../form-content/form-content";
import { StorageProvider } from "../../providers/storage/storage";
import { InspectionProvider } from "../../providers/inspection/inspection";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  showPending = true;
  showCompleted = true;
  showInProgress = true;

  showContent: boolean;
  isOnline: boolean;

  pendingArr: any[] = [];
  inProgressArr: any[] = [];
  completedArr: any[] = [];
  fakeArr: any[] = [];

  segmentTab: string;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    private storage: StorageProvider,
    private inspectionService: InspectionProvider
  ) {
    for (let index = 0; index < 15; index++) {
      this.fakeArr.push(index);
    }

    this.segmentTab = "pending";
    this.isOnline = true;

    this.events.subscribe("app:online",()=>{
      this.isOnline = true;
    });
    this.events.subscribe("app:offline",()=>{
      this.isOnline = false;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad HomePage");
    this.initData();
  }

  private initData(ev?: any) {
    this.showContent = false;

    if (ev) {
      this.events.publish("sync:data:inspection");
    }

    this.events.subscribe("sync:data:inspection:complete", () => {
      getInspections();

      if (ev) {
        this.showContent = true;
        ev.complete();
      }
    });

    const getInspections = () => {
      this.storage
        .getStorage(this.storage.storageKey.INSPECTIONS)
        .then((inspections: any) => {
          const inspectionsArr = inspections;
          _.each(inspectionsArr, (row: any) => {
            row.inspection_date_formatted = moment(row.inspection_date).format(
              "MMM DD"
            );
          });

          this.pendingArr = _.filter(inspectionsArr, (row: any) => {
            return row.request_status === "Pending Inspection";
          });
          this.inProgressArr = _.filter(inspectionsArr, (row: any) => {
            return row.request_status === "In progress";
          });
          this.completedArr = _.filter(inspectionsArr, (row: any) => {
            return row.request_status === "Inspection Finished";
          });

          this.showContent = true;
        });
    };

    getInspections();
  }

  doRefresh(ev) {
    this.initData(ev);
  }

  viewDetails(inspection: any, event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.navCtrl.push(FormContentPage, {
      action: "inspection_details",
      inspection: inspection
    });
  }

  presentActionSheet(inspection: any, event: any) {
    event.preventDefault();
    event.stopPropagation();

    const deleteInspection = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.inspectionService.deleteInspectionInfo(inspection.id).then(
        (data: any) => {
          if (data) {
            this.alertCtrl
              .create({
                title: "SUCCESS",
                subTitle: "Inspection successfully deleted!",
                buttons: ["OK"]
              })
              .present();
            this.initData(null);
          }
          loading.dismiss();
        },
        error => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
    };

    const actionSheet = this.actionSheetCtrl.create({
      title: "Choose action for this inspection",
      buttons: [
        {
          text: "Delete",
          role: "destructive",
          handler: () => {
            const confirm = this.alertCtrl.create({
              title: "Delete this inspection?",
              message: "Confirm to delete this inspection?",
              buttons: [
                {
                  text: "Cancel",
                  handler: () => {}
                },
                {
                  text: "Confirm",
                  handler: () => {
                    deleteInspection();
                  }
                }
              ]
            });
            confirm.present();
          }
        },
        {
          text: "Update",
          handler: () => {
            console.log("Archive clicked");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
}
