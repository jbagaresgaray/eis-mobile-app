import { Injectable } from "@angular/core";
import { Events, Platform } from "ionic-angular";
import { QRScanner } from "@ionic-native/qr-scanner";

/*
  Generated class for the ScanProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ScanProvider {
  public text: string;
  public scannerVisible: boolean;
  public lightEnabled: boolean;
  public frontCameraEnabled: boolean;
  public isDesktop = !this.platform.is("cordova");
  public isAvailable: boolean = true;
  public hasPermission: boolean = false;
  public isDenied: boolean = false;
  public isRestricted: boolean = false;
  public canEnableLight: boolean = false;
  public canChangeCamera: boolean = false;
  public canOpenSettings: boolean = false;
  public backCamera: boolean = true;
  public initializeStarted: boolean = false;
  public initializeCompleted: boolean = false;

  constructor(
    private qrScanner: QRScanner,
    private platform: Platform,
    private events: Events
  ) {
    this.scannerVisible = false;
    this.lightEnabled = false;
    this.frontCameraEnabled = false;
  }

  private checkCapabilities(status) {
    console.log("scannerService is reviewing platform capabilities...");
    // Permission can be assumed on the desktop builds
    this.hasPermission = this.isDesktop || status.authorized ? true : false;
    this.isDenied = status.denied ? true : false;
    this.isRestricted = status.restricted ? true : false;
    this.canEnableLight = status.canEnableLight ? true : false;
    this.canChangeCamera = status.canChangeCamera ? true : false;
    this.canOpenSettings = status.canOpenSettings ? true : false;
    this.logCapabilities();
  }

  private orIsNot(bool: boolean): string {
    return bool ? "" : "not ";
  }

  private logCapabilities() {
    console.log(
      "A camera is " + this.orIsNot(this.isAvailable) + "available to this app."
    );
    var access = "not authorized";
    if (this.hasPermission) access = "authorized";
    if (this.isDenied) access = "denied";
    if (this.isRestricted) access = "restricted";
    console.log("Camera access is " + access + ".");
    console.log(
      "Support for opening device settings is " +
        this.orIsNot(this.canOpenSettings) +
        "available on this platform."
    );
    console.log(
      "A light is " +
        this.orIsNot(this.canEnableLight) +
        "available on this platform."
    );
    console.log(
      "A second camera is " +
        this.orIsNot(this.canChangeCamera) +
        "available on this platform."
    );
  }

  /**
   * Immediately return known capabilities of the current platform.
   */
  public getCapabilities() {
    return {
      isAvailable: this.isAvailable,
      hasPermission: this.hasPermission,
      isDenied: this.isDenied,
      isRestricted: this.isRestricted,
      canEnableLight: this.canEnableLight,
      canChangeCamera: this.canChangeCamera,
      canOpenSettings: this.canOpenSettings
    };
  }

  /**
   * If camera access has been granted, pre-initialize the QRScanner. This method
   * can be safely called before the scanner is visible to improve perceived
   * scanner loading times.
   *
   * The `status` of QRScanner is returned to the callback.
   */
  public gentleInitialize(): Promise<any> {
    return new Promise(resolve => {
      if (this.initializeStarted && !this.isDesktop) {
        this.qrScanner.getStatus().then(status => {
          this.completeInitialization(status);
        });
        return resolve();
      }
      this.initializeStarted = true;
      console.log("Trying to pre-initialize QRScanner.");
      if (!this.isDesktop) {
        this.qrScanner.getStatus().then(status => {
          this.checkCapabilities(status);
          if (status.authorized) {
            console.log("Camera permission already granted.");
            this.initialize().then(() => {
              return resolve();
            });
          } else {
            console.log("QRScanner not authorized, waiting to initalize.");
            this.completeInitialization(status);
            return resolve();
          }
        });
      } else {
        console.log(
          "To avoid flashing the privacy light, we do not pre-initialize the camera on desktop."
        );
        return resolve();
      }
    });
  }

  public reinitialize(): void {
    this.initializeCompleted = false;
    this.qrScanner.destroy();
    this.initialize();
  }

  public initialize(): Promise<any> {
    return new Promise(resolve => {
      console.log("Initializing scanner...");
      this.qrScanner
        .prepare()
        .then(status => {
          this.completeInitialization(status);
          return resolve();
        })
        .catch(err => {
          this.isAvailable = false;
          console.error(err);
          // does not return `status` if there is an error
          this.qrScanner.getStatus().then(status => {
            this.completeInitialization(status);
            return resolve();
          });
        });
    });
  }

  private completeInitialization(status): void {
    this.checkCapabilities(status);
    this.initializeCompleted = true;
    this.events.publish("scannerServiceInitialized");
  }

  public isInitialized(): boolean {
    return this.initializeCompleted;
  }
  public isInitializeStarted(): boolean {
    return this.initializeStarted;
  }

  /**
   * (Re)activate the QRScanner, and cancel the timeouts if present.
   *
   * The `status` of QRScanner is passed to the callback when activation
   * is complete.
   */
  public activate(): Promise<any> {
    return new Promise(resolve => {
      console.log("Activating scanner...");
      this.qrScanner.show().then(status => {
        this.initializeCompleted = true;
        this.checkCapabilities(status);
        return resolve();
      });
    });
  }
  /**
   * Start a new scan.
   */
  public scan(): Promise<any> {
    return new Promise(resolve => {
      console.log("Scanning...");
      let scanSub = this.qrScanner.scan().subscribe((text: string) => {
        console.log("Scan success");
        scanSub.unsubscribe(); // stop scanning
        return resolve(text);
      });
    });
  }

  public pausePreview(): void {
    this.qrScanner.pausePreview();
  }

  public resumePreview(): void {
    this.qrScanner.resumePreview();
  }

  /**
   * Deactivate the QRScanner. To balance user-perceived performance and power
   * consumption, this kicks off a countdown which will "sleep" the scanner
   * after a certain amount of time.
   *
   * The `status` of QRScanner is passed to the callback when deactivation
   * is complete.
   */

  public deactivate(): void {
    console.log("Deactivating scanner...");
    if (this.lightEnabled) {
      this.qrScanner.disableLight();
      this.lightEnabled = false;
    }
    this.hide();
    this.destroy();
  }

  // Natively hide the QRScanner's preview
  // On mobile platforms, this can reduce GPU/power usage
  // On desktop, this fully turns off the camera (and any associated privacy lights)
  private hide() {
    this.qrScanner.hide();
  }

  // Reduce QRScanner power/processing consumption by the maximum amount
  private destroy() {
    this.qrScanner.destroy();
  }

  /**
   * Toggle the device light (if available).
   *
   * The callback receives a boolean which is `true` if the light is enabled.
   */

  public toggleLight(): Promise<any> {
    return new Promise((resolve, reject) => {
      console.log("Toggling light...");
      if (this.lightEnabled) {
        this.qrScanner
          .disableLight()
          .then(() => {
            this.lightEnabled = false;
            return resolve(this.lightEnabled);
          })
          .catch(err => {
            console.error("Scan Provider Error (disableLight)", err);
            return reject(err);
          });
      } else {
        this.qrScanner
          .enableLight()
          .then(() => {
            this.lightEnabled = true;
            return resolve(this.lightEnabled);
          })
          .catch(err => {
            console.error("Scan Provider Error (enableLight)", err);
            return reject(err);
          });
      }
    });
  }

  /**
   * Switch cameras (if a second camera is available).
   *
   * The `status` of QRScanner is passed to the callback when activation
   * is complete.
   */

  public toggleCamera(): Promise<any> {
    return new Promise((resolve, reject) => {
      console.log("Toggling camera...");
      if (this.frontCameraEnabled) {
        this.qrScanner
          .useBackCamera()
          .then(() => {
            this.frontCameraEnabled = false;
            return resolve(this.frontCameraEnabled);
          })
          .catch(err => {
            console.error("Scan Provider Error (useBackCamera)", err);
            return reject(err);
          });
      } else {
        this.qrScanner
          .useFrontCamera()
          .then(() => {
            this.frontCameraEnabled = true;
            return resolve(this.frontCameraEnabled);
          })
          .catch(err => {
            console.error("Scan Provider Error (useFrontCamera)", err);
            return reject(err);
          });
      }
    });
  }

  public openSettings(): void {
    console.log("Attempting to open device settings...");
    this.qrScanner.openSettings();
  }
}
