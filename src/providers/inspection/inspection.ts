import { Injectable } from "@angular/core";
import { Restangular } from "ngx-restangular";

import { AuthProvider } from "../auth/auth";

/*
  Generated class for the InspectionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InspectionProvider {
  constructor(public restangular: Restangular, private auth: AuthProvider) {}

  getAllInspections() {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspection")
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  getInspectionInfo(inspectionId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspection/" + inspectionId)
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  deleteInspectionInfo(inspectionId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspection/" + inspectionId)
        .customDELETE()
        .subscribe(successResponse, errResponse);
    });
  }

  getInspectionBusinessEquipment(inspectionId, businessId, equipmentId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all(
          "inspection/" +
            inspectionId +
            "/business/" +
            businessId +
            "/equipment/" +
            equipmentId
        )
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  getInspectionBusinessEquipmentChecklist(
    inspectionId,
    businessId,
    equipmentId
  ) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all(
          "inspection/" +
            inspectionId +
            "/business/" +
            businessId +
            "/equipment/" +
            equipmentId +
            "/checklistforms"
        )
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  createInspectionBusinessEquipmentChecklist(
    inspectionId,
    businessId,
    equipmentId,
    data
  ) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all(
          "inspection/" +
            inspectionId +
            "/business/" +
            businessId +
            "/equipment/" +
            equipmentId +
            "/checklistforms"
        )
        .customPOST(data)
        .subscribe(successResponse, errResponse);
    });
  }

  saveInspectionInput(data: any) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspectioninput")
        .customPOST(data)
        .subscribe(successResponse, errResponse);
    });
  }
}
