import { Injectable } from "@angular/core";
import { Restangular } from "ngx-restangular";
import { AuthProvider } from "../auth/auth";

@Injectable()
export class InspectionAssetsProvider {
  constructor(public restangular: Restangular, private auth: AuthProvider) {}

  getAllInspectionInputAssets(id) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspectioninput/" + id + "/assets")
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  createAllInspectionInputAssets(id, data) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspectioninput/" + id + "/assets")
        .customPOST(data)
        .subscribe(successResponse, errResponse);
    });
  }

  getInspectionInputAsset(id, assetId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspectioninput/" + id + "/assets/" + assetId)
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  deleteInspectionInputAsset(id, assetId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspectioninput/" + id + "/assets/" + assetId)
        .customDELETE()
        .subscribe(successResponse, errResponse);
    });
  }
}
