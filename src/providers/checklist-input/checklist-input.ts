import { Injectable } from "@angular/core";
import { Restangular } from "ngx-restangular";
import { AuthProvider } from "../auth/auth";

@Injectable()
export class ChecklistInputProvider {
  constructor(public restangular: Restangular, private auth: AuthProvider) {}

  getEquipmentChecklistInput(inspectionId, businessId, equipmentId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all(
          "inspection/" +
            inspectionId +
            "/business/" +
            businessId +
            "/equipment/" +
            equipmentId +
            "/inspectioninput"
        )
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  createEquipmentChecklistInput(inspectionId, businessId, equipmentId, data) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all(
          "inspection/" +
            inspectionId +
            "/business/" +
            businessId +
            "/equipment/" +
            equipmentId +
            "/inspectioninput"
        )
        .customPOST(data)
        .subscribe(successResponse, errResponse);
    });
  }

  getEquipmentChecklistInputDetails(inspectionId, businessId, equipmentId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all(
          "inspection/" +
            inspectionId +
            "/business/" +
            businessId +
            "/equipment/" +
            equipmentId +
            "/inspectioninput"
        )
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  deleteEquipmentChecklistInputDetails(inspectionId, businessId, equipmentId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all(
          "inspection/" +
            inspectionId +
            "/business/" +
            businessId +
            "/equipment/" +
            equipmentId +
            "/inspectioninput"
        )
        .customDELETE()
        .subscribe(successResponse, errResponse);
    });
  }

  updateEquipmentChecklistInputDetails(inspectionId, businessId, equipmentId, data) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all(
          "inspection/" +
            inspectionId +
            "/business/" +
            businessId +
            "/equipment/" +
            equipmentId +
            "/inspectioninput"
        )
        .customPUT(data)
        .subscribe(successResponse, errResponse);
    });
  }
}
