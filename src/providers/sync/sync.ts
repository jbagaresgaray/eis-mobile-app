import { Injectable } from "@angular/core";
import * as _ from "lodash";
import * as async from "async";

import { Events, ItemReorder } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { StorageProvider } from "../../providers/storage/storage";
import { InspectionProvider } from "../../providers/inspection/inspection";
import { ChecklistInputProvider } from "../../providers/checklist-input/checklist-input";
import { InspectionAssetsProvider } from "../../providers/inspection-assets/inspection-assets";

import { environment } from "../../environments/environment";

@Injectable()
export class SyncProvider {
  private checklistInputArr: any[] = [];

  constructor(
    public events: Events,
    public auth: AuthProvider,
    public inspectionService: InspectionProvider,
    public checklistInputService: ChecklistInputProvider,
    private inspectionAssets: InspectionAssetsProvider,
    public _storage: StorageProvider
  ) {}

  toDataUrl(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      const reader = new FileReader();
      reader.onloadend = function() {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open("GET", url);
    xhr.responseType = "blob";
    xhr.send();
  }

  async downloadCurrentUser() {
    await this.auth.getCurrentUser().then((data: any) => {
      const response = data.response;
      if (response && response.data) {
        localStorage.setItem(
          this._storage.storageKey.CURRENT_USER,
          JSON.stringify(response.data)
        );
      }
    });
  }

  async downloadInspections() {
    await this.inspectionService.getAllInspections().then((data: any) => {
      const resp = data.response;
      if (resp && resp.data) {
        if (!_.isEmpty(resp.data.inspections)) {
          const inspectionsArr = resp.data.inspections.resp;
          this._storage.setStorage(
            this._storage.storageKey.INSPECTIONS,
            inspectionsArr
          );
          this.events.publish("sync:data:inspection:complete");
        }
      }
    });
  }

  async downloadAllData() {
    await this.inspectionService.getAllInspections().then(
      (data: any) => {
        const resp = data.response;
        if (resp && resp.data) {
          if (!_.isEmpty(resp.data.inspections)) {
            const inspectionsArr = resp.data.inspections.resp;
            this._storage.setStorage(
              this._storage.storageKey.INSPECTIONS,
              inspectionsArr
            );
            this.events.publish("sync:data:inspection:complete");

            async.each(inspectionsArr, (item, callback) => {
              const inspectionId = item.id;
              console.log("inspectionId: ", inspectionId);
              this.inspectionService.getInspectionInfo(inspectionId).then(
                (data: any) => {
                  const resp = data.response;
                  if (resp) {
                    const details = resp.data.resp;
                    if (details) {
                      this._storage.setStorage(
                        this._storage.storageKey.INSPECTION_DETAIL +
                          "_" +
                          inspectionId,
                        details
                      );

                      if (details.equipments) {
                        async.eachSeries(
                          details.equipments,
                          (equipment, cb) => {
                            async.parallel(
                              [
                                callback1 => {
                                  this.downloadInspectionEquipment(
                                    details.id,
                                    details.business_id,
                                    equipment.equipment_id
                                  ).then(() => {
                                    callback1();
                                  });
                                },
                                callback1 => {
                                  this.downloadInspectionEquipmentChecklist(
                                    inspectionId,
                                    details.business_id,
                                    equipment.equipment_id
                                  ).then(() => {
                                    callback1();
                                  });
                                },
                                callback1 => {
                                  this.downloadChecklistInputData(
                                    inspectionId,
                                    details.business_id,
                                    equipment.equipment_id
                                  ).then(() => {
                                    callback1();
                                  });
                                }
                              ],
                              () => {
                                cb();
                              }
                            );
                          },
                          () => {
                            console.log(
                              "this.checklistInputArr: ",
                              this.checklistInputArr
                            );
                            callback();
                            this.events.publish("sync:data:complete");
                          }
                        );
                      } else {
                        this.events.publish("sync:data:complete");
                        callback();
                      }
                    } else {
                      this.events.publish("sync:data:complete");
                      callback();
                    }
                  } else {
                    this.events.publish("sync:data:complete");
                    callback();
                  }
                },
                () => {
                  this.events.publish("sync:data:inspection_detail:complete");
                }
              );
            });
          }
        }
      },
      () => {
        this.events.publish("sync:data:inspection:complete");
      }
    );
  }

  async downloadInspectionEquipment(inspectionId, businessId, equipmentId) {
    console.log("downloadInspectionEquipment");
    await this.inspectionService
      .getInspectionBusinessEquipment(inspectionId, businessId, equipmentId)
      .then(
        (data: any) => {
          const response = data.response;
          const respData = response.data;
          if (respData) {
            const storeKey =
              this._storage.storageKey.EQUIPMENT_CHECKLIST +
              "_" +
              inspectionId +
              "_" +
              businessId +
              "_" +
              equipmentId;
            this._storage.setStorage(storeKey, respData.resp);
          }
          this.events.publish("sync:data:inspection_equipment:complete");
        },
        error => {
          this.events.publish("sync:data:inspection_equipment:complete");
        }
      );
  }

  async downloadInspectionEquipmentChecklist(
    inspectionId,
    businessId,
    equipmentId
  ) {
    console.log("downloadInspectionEquipmentChecklist");
    await this.inspectionService
      .getInspectionBusinessEquipmentChecklist(
        inspectionId,
        businessId,
        equipmentId
      )
      .then(
        (data: any) => {
          const response = data.response;
          const respData = response.data;
          if (respData) {
            const storeKey =
              this._storage.storageKey.CHECKLISTS +
              "_" +
              inspectionId +
              "_" +
              businessId +
              "_" +
              equipmentId;
            this._storage.setStorage(storeKey, respData.resp);
          }
          this.events.publish("sync:data:inspection_checklist:complete");
        },
        error => {
          this.events.publish("sync:data:inspection_checklist:complete");
        }
      );
  }

  async downloadChecklistInputData(inspectionId, businessId, equipmentId) {
    await this.checklistInputService
      .getEquipmentChecklistInput(inspectionId, businessId, equipmentId)
      .then((data: any) => {
        const response = data.response;
        const respData = response.data;
        if (respData) {
          this.checklistInputArr = respData.inspectioninputs.resp;
          console.log("this.checklistInputArr: ", this.checklistInputArr);
          const storeKey =
            this._storage.storageKey.CHECKLISTS_INPUT +
            "_" +
            inspectionId +
            "_" +
            businessId +
            "_" +
            equipmentId;
          this._storage.setStorage(storeKey, respData);

          if (this.checklistInputArr) {
            _.each(this.checklistInputArr, (item: any) => {
              this.inspectionAssets
                .getAllInspectionInputAssets(item.id)
                .then((assets: any) => {
                  item.uploaded = assets.response.data.resp;
                  console.log("item.uploaded: ", item.uploaded);
                  if (!_.isEmpty(item.uploaded)) {
                    const storeKey1 =
                      this._storage.storageKey.CHECKLISTS_INPUT_IMAGE +
                      "_" +
                      item.id;
                    async.eachSeries(
                      item.uploaded,
                      (row: any, cb) => {
                        const imagepath = environment.api_url + "/" + row.path;
                        this.toDataUrl(imagepath, myBase64 => {
                          // myBase64 is the base64 string
                          row.public_path = myBase64;
                          cb();
                        });
                      },
                      () => {
                        this._storage.setStorage(
                          storeKey1,
                          JSON.stringify(item.uploaded)
                        );
                      }
                    );
                  }
                });
            });
          }
        }
        this.events.publish("sync:data:inspection_checklist_input:complete");
      });
  }

  async downloadInspectionDetail(inspectionId) {
    await this.inspectionService.getInspectionInfo(inspectionId).then(
      (data: any) => {
        const resp = data.response;
        if (resp) {
          const details = resp.data.resp;
          if (details) {
            this._storage.setStorage(
              this._storage.storageKey.INSPECTION_DETAIL + "_" + inspectionId,
              details
            );

            if (details.equipments) {
              async.eachSeries(
                details.equipments,
                (equipment, cb) => {
                  async.parallel(
                    [
                      callback => {
                        this.downloadInspectionEquipment(
                          details.id,
                          details.business_id,
                          equipment.equipment_id
                        ).then(() => {
                          callback();
                        });
                      },
                      callback => {
                        this.downloadInspectionEquipmentChecklist(
                          inspectionId,
                          details.business_id,
                          equipment.equipment_id
                        ).then(() => {
                          callback();
                        });
                      },
                      callback => {
                        this.downloadChecklistInputData(
                          inspectionId,
                          details.business_id,
                          equipment.equipment_id
                        ).then(() => {
                          callback();
                        });
                      }
                    ],
                    () => {
                      cb();
                    }
                  );
                },
                () => {
                  this.events.publish("sync:data:inspection_detail:complete");
                }
              );
            }
          }
        }
      },
      () => {
        this.events.publish("sync:data:inspection_detail:complete");
      }
    );
  }
}
