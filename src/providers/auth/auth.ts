import { Injectable } from "@angular/core";
import { Restangular } from "ngx-restangular";
import { JwtHelperService } from "@auth0/angular-jwt";
import * as _ from "lodash";
import { Platform } from "ionic-angular";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  public splitPaneState: boolean;
  public isOnline: boolean;

  constructor(
    public restangular: Restangular,
    public jwtHelper: JwtHelperService,
    public platform: Platform
  ) {
    this.splitPaneState = true;
  }

  setSplitPane(state: boolean) {
    if (this.platform.is("tablet")) {
      this.splitPaneState = state;
    } else {
      this.splitPaneState = false;
    }
  }

  getSplitPane() {
    return this.splitPaneState;
  }

  authenticate(data: any) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };

      var authdata = window.btoa(data.username + ":" + data.password);
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Basic " + authdata
          });
        })
        .all("login")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  public isAuthenticated(): boolean {
    const token = this.getUserToken();
    if (_.isEmpty(token)) {
      return false;
    } else {
      return !this.jwtHelper.isTokenExpired(token);
    }
  }

  logoutApp() {
    localStorage.removeItem("app.eis.token");
    localStorage.removeItem("app.eis.user");
    localStorage.removeItem("app.eis.current_user");
  }

  getUserToken() {
    return localStorage.getItem("app.eis.token");
  }

  saveUserToken(token) {
    localStorage.setItem("app.eis.token", token);
  }

  saveUserInfo(user) {
    localStorage.setItem("app.eis.user", JSON.stringify(user));
  }

  getUserInfo() {
    return localStorage.getItem("app.eis.user");
  }

  getCurrentUser() {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.getUserToken()
          });
        })
        .all("user/me")
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }
}
