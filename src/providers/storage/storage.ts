import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";

@Injectable()
export class StorageProvider {
  public storageKey = {
    USERS: "users_table",
    CURRENT_USER: "app.eis.current_user",
    INSPECTIONS: "inspections",
    INSPECTION_DETAIL: "inspection_detail",
    EQUIPMENT_CHECKLIST: "equipment_checklist",
    CHECKLISTS: "checklists",
    CHECKLISTS_INPUT: "checklists_input",
    CHECKLISTS_INPUT_IMAGE: "checklists_input_image",
    CHECKLIST_DRAFT: "checklist_draft",
    SCANNED_INSPECTION: "scanned_inspection"
  };

  constructor(private storage: Storage) {}

  async setStorage(key, value) {
    await this.storage.set(key, value);
  }

  async getStorage(key) {
    return await this.storage.get(key).then(val => {
      return val;
    });
  }

  async removeStorage(key) {
    await this.storage.remove(key);
  }

  async cleanStorage() {
    this.storage.clear();
  }
}
