import { Injectable } from "@angular/core";

import { Network } from "@ionic-native/network";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from "rxjs/Observable";

export enum ConnectionStatus {
  Online,
  Offline
}

@Injectable()
export class NetworkProvider {
  public status: ConnectionStatus;
  private _status: BehaviorSubject<ConnectionStatus> = new BehaviorSubject(
    null
  );

  constructor(private network: Network) {
    this.status = ConnectionStatus.Online;
    this.setStatus(this.status);
  }

  public initializeNetworkEvents(): void {
    /* OFFLINE */
    this.network.onDisconnect().subscribe(() => {
      console.log("onDisconnect: ", this.status);
      if (this.status === ConnectionStatus.Online) {
        this.setStatus(ConnectionStatus.Offline);
      }
    });

    /* ONLINE */
    this.network.onConnect().subscribe(() => {
      console.log("onConnect: ", this.status);
      if (this.status === ConnectionStatus.Offline) {
        this.setStatus(ConnectionStatus.Online);
      }
    });
  }

  public getNetworkType(): string {
    return this.network.type;
  }

  public getNetworkStatus(): Observable<ConnectionStatus> {
    return this._status.asObservable();
  }

  private setStatus(status: ConnectionStatus) {
    this.status = status;
    console.log("this.status: ", this.status);
    this._status.next(this.status);
  }
}
