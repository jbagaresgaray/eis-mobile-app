import { Injectable } from "@angular/core";
import { Restangular } from "ngx-restangular";
import { AuthProvider } from "../auth/auth";

@Injectable()
export class InspectionTrackingProvider {
  constructor(public restangular: Restangular, private auth: AuthProvider) {}

  getAllInspectionsTracking(inspectionId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspection/" + inspectionId + "/tracking")
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  createAllInspectionsTracking(inspectionId, data) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspection/" + inspectionId + "/tracking")
        .customPOST(data)
        .subscribe(successResponse, errResponse);
    });
  }

  getAllInspectionsTrackingDetails(inspectionId, trackingId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspection/" + inspectionId + "/tracking/" + trackingId)
        .customGET()
        .subscribe(successResponse, errResponse);
    });
  }

  deleteAllInspectionsTrackingDetails(inspectionId, trackingId) {
    return new Promise((resolve, reject) => {
      const successResponse = resp => {
        resolve(resp);
      };

      const errResponse = error => {
        reject(error);
      };

      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + this.auth.getUserToken()
          });
        })
        .all("inspection/" + inspectionId + "/tracking/" + trackingId)
        .customDELETE()
        .subscribe(successResponse, errResponse);
    });
  }
}
